# Sobre

Este projeto permite realizar a autenticação de um usuário, pesquisar, listar e detalhar empresas.
Com mais tempo para o desenvolver o projeto iriar adicionar mais funcionalidades como: 

- Capturar a sessão de Login

- Implementar o Room para armazenar os dados

- Realizar testes unitários

## Instruções de uso
1 - Importar o projeto no Android Studio.

2 - Compilar e executar o aplicativo.

## Bibliotecas utilizadas

### Coroutines
Como o projeto faz consumo de API é necessario aguardar a resposta dos endpoints.

### Dagger - Hilt
Utilizado como injeção de dependência no projeto por ser uma biblioteca de fácil uso.

### Data Binding
Responsável por vincular o modelo de Enterprise ao Layout XML para exibir os detalhes das empresas.

### Glide
Utilizado para realizar o carregamento da imagem da empresa.

### Gson
Responsável por converter o Json de retorno da API para Objetos Kotlin.

### Lifecycle
O projeto utiliza o LiveData para armazenar as empresas pesquisadas e observar o seu estado em tempo real.

### Navegation Component
Utilizado para navegação de fragmentos no projeto.

### Retrofit
Responsável por consumir a api do projeto para a realização do login, listagem e detalhes das empresas.