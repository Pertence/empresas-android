package com.example.app.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.app.R
import com.example.app.databinding.ActivityLoginBinding
import com.example.app.ui.MainActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    private val viewModel: LoginViewModel by viewModels()

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupLogin()

    }

    private fun setupLogin() {
        binding.apply {

            buttonLogin.setOnClickListener {
                binding.progressBar.visibility = View.VISIBLE
                if (viewModel.checkNullFields(
                        editEmail.text.toString(),
                        editPassword.text.toString()
                    )
                ) {
                    binding.progressBar.visibility = View.INVISIBLE
                    response.error = getString(R.string.error_field)
                    buttonLogin.setBackgroundResource(R.color.lightMediumGray)
                } else {
                    lifecycleScope.launch {
                        val resultLogin = viewModel.postLogin(
                            editEmail.text.toString(),
                            editPassword.text.toString()
                        )
                        if (resultLogin) {
                            startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                            finish()
                        } else {
                            buttonLogin.setBackgroundResource(R.color.lightMediumGray)
                            binding.progressBar.visibility = View.INVISIBLE
                            response.error = getString(R.string.error_credentials)
                        }

                    }

                }
            }

        }
    }

}