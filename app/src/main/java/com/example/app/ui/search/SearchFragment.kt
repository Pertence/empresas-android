package com.example.app.ui.search

import android.os.Bundle
import android.view.*
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.app.R
import com.example.app.databinding.FragmentSearchBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SearchFragment : Fragment() {

    private val viewModel: SearchViewModel by viewModels()

    private val adapter by lazy { SearchAdapter() }

    lateinit var binding: FragmentSearchBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentSearchBinding.inflate(inflater, container, false)

        setupRecyclerView()

        if (adapter.itemCount == 0) {
            binding.textInformation.visibility = View.VISIBLE
        } else {
            binding.textInformation.visibility = View.INVISIBLE
        }

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_toolbar, menu)
        val search = menu.findItem(R.id.search_company)
        val searchView = search.actionView as androidx.appcompat.widget.SearchView
        searchView.queryHint = getString(R.string.search)

        setupSearch(searchView)

        super.onCreateOptionsMenu(menu, inflater)

    }

    private fun setupRecyclerView() {
        binding.recyclerviewSearch.adapter = adapter
        viewModel.enterpriseList.observe(viewLifecycleOwner, { enterprises ->
            if (enterprises.isEmpty()) {
                binding.textNoResult.visibility = View.VISIBLE
                adapter.submitList(null)
            } else {
                adapter.submitList(enterprises)
            }
        })
    }

    private fun setupSearch(searchView: androidx.appcompat.widget.SearchView){
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                lifecycleScope.launch {
                    viewModel.getEnterprise(query.toString())
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                binding.textInformation.visibility = View.INVISIBLE
                binding.textNoResult.visibility = View.INVISIBLE
                return true
            }

        })
    }

}