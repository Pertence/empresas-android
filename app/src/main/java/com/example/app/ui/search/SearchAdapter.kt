package com.example.app.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.app.data.model.Enterprise
import com.example.app.databinding.RowEnterpriseItemBinding

class SearchAdapter : ListAdapter<Enterprise, SearchAdapter.SearchViewHolder>(DiffUtilCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val binding =
            RowEnterpriseItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SearchViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class SearchViewHolder(private val binding: RowEnterpriseItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(enterprise: Enterprise) {
            binding.enterprise = enterprise

            binding.rowItemEnterprise.setOnClickListener {
                val action = SearchFragmentDirections.actionSearchFragmentToDetailFragment(enterprise)
                binding.rowItemEnterprise.findNavController().navigate(action)
            }
        }
    }

}

private object DiffUtilCallback : DiffUtil.ItemCallback<Enterprise>() {
    override fun areItemsTheSame(oldItem: Enterprise, newItem: Enterprise): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Enterprise, newItem: Enterprise): Boolean =
        oldItem == newItem
}