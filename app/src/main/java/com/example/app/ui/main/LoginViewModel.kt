package com.example.app.ui.main

import androidx.lifecycle.ViewModel
import com.example.app.data.repository.AppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val repository: AppRepository
) : ViewModel() {

    suspend fun postLogin(email: String, password: String): Boolean =
        repository.postLogin(email, password)

    fun checkNullFields(email: String, password: String): Boolean {
        if (email.trim().isEmpty() || password.trim().isEmpty()) {
            return true
        }
        return false
    }

}