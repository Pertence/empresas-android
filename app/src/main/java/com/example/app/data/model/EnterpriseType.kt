package com.example.app.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class EnterpriseType(
    val enterprise_type_name: String,
    val id: Int
): Parcelable