package com.example.app.data.model

data class LoginResponse(
    val success: Boolean
)