package com.example.app.data.service

import okhttp3.Interceptor
import okhttp3.Response

class LoginInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val url = chain.request().url().newBuilder()
            .addQueryParameter("access-token", ACCESS_TOKEN)
            .addQueryParameter("client", CLIENT)
            .addQueryParameter("uid", UID)
            .build()

        val request = chain.request().newBuilder()
            .url(url)
            .build()

        return chain.proceed(request)
    }

    companion object{
        var ACCESS_TOKEN = ""
        var CLIENT = ""
        var UID = ""
    }

}
