package com.example.app.data.service

import com.example.app.data.model.EnterpriseList
import com.example.app.data.model.LoginResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface RetrofitService {

    @POST("users/auth/sign_in")
    suspend fun postLogin(
        @Query("email") email: String,
        @Query("password") password: String
    ): Response<LoginResponse>?

    @GET("enterprises")
    suspend fun searchCompany(
        @Query("name") name: String
    ): Response<EnterpriseList>?

}