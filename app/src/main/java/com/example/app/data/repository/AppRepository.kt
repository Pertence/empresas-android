package com.example.app.data.repository

import com.example.app.data.model.Enterprise
import com.example.app.data.service.LoginInterceptor
import com.example.app.data.service.RetrofitService

class AppRepository(private var retrofitService: RetrofitService) {

    suspend fun postLogin(email: String, password: String): Boolean {

        val response = retrofitService.postLogin(email, password)
        val body = response?.body()
        if (body?.success == true) {
            LoginInterceptor.ACCESS_TOKEN = response.headers()["access-token"].toString()
            LoginInterceptor.CLIENT = response.headers()["client"].toString()
            LoginInterceptor.UID = response.headers()["uid"].toString()
            return true
        }
        return false
    }

    suspend fun getEnterprise(name: String): List<Enterprise> {
        lateinit var enterpriseList: List<Enterprise>

        try {
            val response = retrofitService.searchCompany(name)
            val body = response?.body()
            if (body != null) {
                enterpriseList = body.enterprises
            }
        } catch (exception: Exception) {
            enterpriseList = listOf()
        }
        return enterpriseList
    }

}