package com.example.app.data.model

data class EnterpriseList(
    val enterprises: List<Enterprise>
)