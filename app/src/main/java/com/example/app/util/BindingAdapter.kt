package com.example.app.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.app.data.model.Enterprise
import com.example.app.util.Constants.Companion.BASE_URL_IMAGE

@BindingAdapter("loadImage")
fun loadImage(imageView: ImageView, enterprise: Enterprise) {
    val imageUrl = BASE_URL_IMAGE + enterprise.photo
    Glide.with(imageView)
        .load(imageUrl)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(imageView)
}