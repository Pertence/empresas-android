package com.example.app.util

class Constants {
    companion object {
        const val BASE_URL = "https://empresas.ioasys.com.br/api/v1/"
        const val BASE_URL_IMAGE = "https://empresas.ioasys.com.br"
    }
}