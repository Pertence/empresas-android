package com.example.app.di

import com.example.app.data.service.RetrofitService
import com.example.app.data.repository.AppRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun providerRepository(
        retrofitService: RetrofitService
    ): AppRepository {
        return AppRepository(retrofitService)
    }

}